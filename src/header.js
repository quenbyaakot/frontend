
import React, { Component } from 'react';
import {
 Box,
 Button,
 Collapsible,
 Heading,
 Grommet,
 Grid,
 Layer,
 ResponsiveContext,
 Anchor,
} from 'grommet';
import { theme } from './App';
import Cookies from 'js-cookie';
import { FormClose, Menu, User, Logout } from 'grommet-icons';

const HeaderBar = (props) => (
 <Box
   tag='header'
   direction='row'
   align='center'
   justify='between'
   background='brand'
   pad={{ left: 'medium', right: 'small', vertical: 'small' }}
   elevation='medium'
   style={{ zIndex: '1' }}
   {...props}
 />
);

function logout() {
  Cookies.remove('fsessid');
  document.location.href='/';
}

class Header extends Component {
 state = {
   showSidebar: false,
 }

 render() {
    const { showSidebar } = this.state;
    if (Cookies.get('fsessid')) {
      return (
        <HeaderBar>
          <Anchor href="/" color="#F2F2F2" className="header-margin-link">
            <Heading level='3' margin='none'>
                RedBull
            </Heading>
          </Anchor>

          <Box direction="row">
            <Anchor href="/servers/" color="#F8F8F8" margin='10px' primary label="Servers" />
            <Anchor href="/scoreboard/" color="#F8F8F8" margin='10px' primary label="Scoreboard" />
          </Box>

          <Box direction="row" className="header-margin-link">
            {/* <Anchor href="/profile/" color="#F8F8F8" margin='auto' primary label="Profile" icon={<User color='#f8f8f8'/>} /> */}
            <Anchor onClick={logout} color="#F8F8F8" margin='10px' primary label="Logout" />
          </Box>
        </HeaderBar>
      );
    } else
      return (
        <HeaderBar>
          <Anchor href="/" color="#F2F2F2" className="header-margin-link">
            <Heading level='3' margin='none'>
                RedBull
            </Heading>
          </Anchor>

          <Box direction="row">
            <Anchor href="/servers/" color="#F8F8F8" margin='10px' primary label="Servers" />
            <Anchor href="/scoreboard/" color="#F8F8F8" margin='10px' primary label="Scoreboard" />
          </Box>

          <Box direction="row" className="header-margin-link">
            {/* <Anchor href="/profile/" color="#F8F8F8" margin='auto' primary label="Profile" icon={<User color='#f8f8f8'/>} /> */}
            <Anchor href="/login/" color="#F8F8F8" margin='10px' primary label="Login" />
          </Box>
        </HeaderBar>
      );
 }
}

export default Header;
