
import React, { Component } from 'react';
import {
 Box,
 Button,
 Collapsible,
 Heading,
 Grommet,
 Layer,
 ResponsiveContext,
} from 'grommet';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from "react-router-dom";
import { FormClose, Menu } from 'grommet-icons';
import Header from './header'
import Footer from './footer'
import Main from './pages/main'
import Profile from './pages/profile'
import Scoreboard from './pages/scoreboard'
import Servers from './pages/servers'
import Login from './pages/login'

const theme = {
 global: {
   colors: {
     brand: '#00739D',
   },
   font: {
     family: 'Roboto',
     size: '14px',
     height: '20px',
   },
 },
};

class App extends Component {
 render() {
   return (
     <Grommet theme={theme} full>
       <Header />

       <Router>
        <Switch>

          <Route path="/servers">
            <Servers />
          </Route>

          <Route path="/scoreboard">
            <Scoreboard />
          </Route>

          <Route path="/profile">
            <Profile />
          </Route>

          <Route path="/login/">
            <Login />
          </Route>

          <Route path="/">
            <Main />
          </Route>

        </Switch>
       </Router>

       <Footer />
     </Grommet>
   );
 }
}

export default App;
export { theme };