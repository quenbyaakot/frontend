
import React, { Component } from 'react';
import {
 Box,
 Button,
 Collapsible,
 Heading,
 Grommet,
 Grid,
 Layer,
 ResponsiveContext,
 Anchor,
} from 'grommet';
import { theme } from '../App';
import { FormClose, Menu, User } from 'grommet-icons';


class Main extends Component {

 render() {
   return (
        <Box
            direction="row"
            border={{ size: 'small' }}
            pad="medium"
        >
            <p>Main</p>
        </Box>
   );
 }
}

export default Main;
