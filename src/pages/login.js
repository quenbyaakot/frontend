
import React, { Component } from 'react';
import {
 Box,
 Button,
 Collapsible,
 Heading,
 Grommet,
 Grid,
 Layer,
 ResponsiveContext,
 Anchor,
 Form,
 FormField,
} from 'grommet';

import Cookies from 'js-cookie';
import { theme } from '../App';
import { FormClose, Menu, User } from 'grommet-icons';

function send(event) {
  var username = event.value['username'];
  var password = event.value['password'];
  
  // request to backend

  var el = document.getElementById('form-error');
  if (username == password) {
    el.textContent = "Success!"
    el.style = 'color: green';
    Cookies.set('fsessid', 'some_sessid')
    window.location.href = '/'

  } else {
    el.textContent = "Wrong credentials!"
    el.style = 'color: red';
  }

}

class Login extends Component {

 render() {
    if (Cookies.get('fsessid')) {
      window.location.href = '/'
    }
    return (
      <Box
          direction="row"
          border={{ size: 'small' }}
          pad="medium"
      > 
        <Grid  style={{ width: '100%', height: '100vh' }}>
          <Heading level='3' margin='auto'>Login</Heading>

          <Form style={{ width: '50%', margin: '30px auto' }} onSubmit={ send }>
            <p id="form-error"></p>
            <FormField required name="username" label="Username/Email" />
            <FormField required type='password' name="password" label="Password" />
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                <Button type="submit" margin='0 auto' label="Submit" />
            </div>
          </Form>
        </Grid>
      </Box>
   );
 }
}

export default Login;
