
import React, { Component } from 'react';
import {
 Box,
 Button,
 Collapsible,
 Heading,
 Grommet,
 Grid,
 Layer,
 ResponsiveContext,
 Anchor,
} from 'grommet';
import { theme } from '../App';
import { FormClose, Menu, User } from 'grommet-icons';


class Scoreboard extends Component {

 render() {
   return (
      <Box
          direction="row"
          border={{ size: 'small' }}
          pad="medium"
      >
          <p>Scoreboard</p>
      </Box>
   );
 }
}

export default Scoreboard;
