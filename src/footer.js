
import React, { Component } from 'react';
import {
 Box,
 Button,
 Collapsible,
 Heading,
 Grommet,
 Grid,
 Layer,
 ResponsiveContext,
 Anchor,
} from 'grommet';
import {  theme } from './header'
import { FormClose, Menu, User } from 'grommet-icons';

const FooterBar = (props) => (
    <Box
      tag='footer'
      direction='row'
      align='center'
      justify='between'
      background='brand'
      pad={{ left: 'medium', right: 'small', vertical: 'small' }}
      elevation='medium'
      style={{ zIndex: '1' }}
      {...props}
    />
);


class Footer extends Component {
 state = {
   showSidebar: false,
 }

 render() {
   const { showSidebar } = this.state;
   return (
        <FooterBar>
          <p style={{ textAlign: 'center', width: '100%' }}>RedBull, 2079</p>
        </FooterBar>
   );
 }
}

export default Footer;
